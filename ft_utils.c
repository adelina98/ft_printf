int	ft_isdigit(int c)
{
	if (c >= 48 && c <= 57)
		return (1);
	return (0);
}

int	ft_putchar(char c)
{
	write(1, &c, 1);
	return (1);
}

int	padding_before(t_padding padding)
{
	int	count;

	count = 0;
	while (padding.before > 0)
	{
		ft_putchar(' ');
		count++;
		padding.before--;
	}
	return (count);
}

int	padding_after(t_padding padding)
{
	int	count;

	count = 0;
	while (padding.after > 0)
	{
		ft_putchar(' ');
		count++;
		padding.after--;
	}
	return (count);
}

int	padding_zero(t_padding padding)
{
	int	count;

	count = 0;
	while (padding.zero > 0)
	{
		ft_putchar('0');
		count++;
		padding.zero--;
	}
	return (count);
}

int	ft_putstr_number(char *str, t_padding padding)
{
	int		index;
	int		count;

	index = 0;
	count = 0;
	if (str[index] == '-')
	{
		ft_putchar('-');
		count++;
		index++;
	}
	while (padding.zero > 0)
	{
		ft_putchar('0');
		count++;
		padding.zero--;
	}
	while (str[index] != '\0')
	{
		ft_putchar(str[index]);
		index++;
		count++;
	}
	return (count);
}


char	*ft_strchr(const char *s, int c)
{
	size_t	i;

	i = 0;
	while (s[i] != '\0')
	{
		if (s[i] == c)
			return ((char *)&s[i]);
		i++;
	}
	if (c == '\0')
		return ((char *)&s[i]);
	return (NULL);
}

char	*ft_strdup(const char *s)
{
	int		i;
	char	*str;

	i = 0;
	if (!(str = (char*)malloc(sizeof(char) * (ft_strlen(s) + 1))))
		return (NULL);
	while (s[i])
	{
		str[i] = s[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!s1 || !s2)
		return (NULL);
	if (!(str = malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1))))
		return (NULL);
	while (s1[i])
	{
		str[i] = s1[i];
		i++;
	}
	while (s2[j])
		str[i++] = s2[j++];
	str[i] = '\0';
	return (str);
}

int	ft_strlen(const char *str)
{
	int		index;

	index = 0;
	while (str[index] != '\0')
		index++;
	return (index);
}

int	ft_strlen_neg(const char *str)
{
	int		index;
	int		count;

	index = 0;
	count = 0;
	if (str[index] == '-')
		index++;
	while (str[index++] != '\0')
		count++;
	return (count);
}

char	*ft_substr(char const *s, int start, int len)
{
	int		i;
	char	*str;

	if (!s)
		return (NULL);
	if (ft_strlen(s) <= start)
		return (ft_strdup(""));
	if (len > (ft_strlen(s) - start))
		len = ft_strlen(s) - start;
	i = 0;
	if (!(str = (char*)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		str[i] = s[start + i];
		i++;
	}
	str[i] = '\0';
	return (str);
}


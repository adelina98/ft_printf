#include "libftprintf.h"

//put c

t_padding		set_flags_c(t_arg flag, t_padding padding)
{
	if (flag.zero && flag.width && !flag.minus)
		padding.zero = flag.width - 1;
	else
	{
		if (!flag.minus)
			padding.before = flag.width - 1;
		if (flag.minus)
			padding.after = flag.width - 1;
		if (flag.width < 0)
		{
			flag.width *= -1;
			padding.after = flag.width - 1;
		}
	}
	return (padding);
}

int				ft_put_c(unsigned char c, t_arg flag)
{
	int			count;
	t_padding	padding;

	padding = set_flags_c(flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putchar(c);
	count += padding_after(padding);
	return (count);
}

int				ft_put_percent(t_arg flag)
{
	int			count;
	t_padding	padding;

	padding = set_flags_c(flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putstr_number("%", padding);
	count += padding_after(padding);
	return (count);
}

int				ft_put_nothing(t_arg flag)
{
	int			count;
	t_padding	padding;

	padding = set_flags_c(flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += padding_zero(padding);
	count += padding_after(padding);
	return (count);
}

put d_i

t_padding	ft_set_flags_number(void)
{
	t_padding	padding;

	padding.before = 0;
	padding.zero = 0;
	padding.after = 0;
	return (padding);
}

t_arg		get_negative(t_arg flag)
{
	if (flag.width < 0)
	{
		flag.minus = 1;
		flag.width *= (-1);
	}
	return (flag);
}

t_padding	set_neg_minus(t_arg flag, t_padding padding)
{
	if (flag.minus)
	{
		padding.after = padding.before;
		padding.before = 0;
	}
	return (padding);
}

t_padding	set_flags(char **to_fill, t_arg flag, t_padding padding)
{
	flag = get_negative(flag);
	if (flag.zero && flag.width && !flag.minus && !(flag.dot
		&& flag.precision >= 0))
		padding.zero = flag.width - ft_strlen(*to_fill);
	else if (flag.precision >= flag.width)
	{
		flag.width = 0;
		if (flag.precision >= ft_strlen(*to_fill))
			padding.zero = flag.precision - ft_strlen_neg(*to_fill);
	}
	else if (flag.precision < flag.width)
	{
		if (flag.precision >= ft_strlen(*to_fill))
		{
			padding.before = flag.width - flag.precision;
			if (ft_strchr(*to_fill, '-'))
				padding.before--;
			padding.zero = flag.precision - ft_strlen_neg(*to_fill);
		}
		else
			padding.before = flag.width - ft_strlen(*to_fill);
		padding = set_neg_minus(flag, padding);
	}
	return (padding);
}

int			ft_put_d_i(int num, t_arg flag)
{
	int			count;
	char		*to_fill;
	t_padding	padding;
	char		*tmp;

	if (!(to_fill = ft_itoa(num)))
		return (-1);
	if (num == 0 && flag.precision == 0 && flag.dot == 1)
	{
		tmp = to_fill;
		if (!(to_fill = ft_strdup("")))
		{
			free(tmp);
			return (-1);
		}
		free(tmp);
	}
	padding = set_flags(&to_fill, flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putstr_number(to_fill, padding);
	count += padding_after(padding);
	free(to_fill);
	return (count);
}


//put ptr

char			*ft_itoa_p(void *p)
{
	char				*result;
	char				*tmp;
	char				hex;
	unsigned long int	num;

	num = (unsigned long int)p;
	if (!(result = ft_strdup("")))
		return (NULL);
	while (num != 0)
	{
		if (num % 16 < 10)
			hex = num % 16 + 48;
		else
			hex = num % 16 + 55 + 32;
		tmp = result;
		if (!(result = ft_strjoin_x(hex, result)))
		{
			free(tmp);
			return (NULL);
		}
		free(tmp);
		num /= 16;
	}
	return (result);
}

t_padding		set_flags_ptr(char **to_fill, t_arg flag, t_padding padding)
{
	flag = get_negative(flag);
	if (flag.zero && flag.width && !flag.minus && !flag.dot)
		padding.zero = flag.width - ft_strlen(*to_fill) - 2;
	else if (flag.precision >= flag.width)
	{
		flag.width = 0;
		if (flag.precision > ft_strlen(*to_fill))
			padding.zero = flag.precision - ft_strlen_neg(*to_fill);
	}
	else if (flag.precision < flag.width)
	{
		if (flag.precision > ft_strlen(*to_fill))
		{
			padding.before = flag.width - flag.precision - 2;
			padding.zero = flag.precision - ft_strlen(*to_fill);
		}
		else
			padding.before = flag.width - ft_strlen(*to_fill) - 2;
		padding = set_neg_minus(flag, padding);
	}
	return (padding);
}

int				ft_ptr_null(t_arg flag)
{
	int					count;
	char				*to_fill;
	t_padding			padding;

	count = 0;
	if (flag.dot && !(to_fill = ft_strdup("0x")))
		return (-1);
	else if (!flag.dot && !(to_fill = ft_strdup("0x0")))
		return (-1);
	padding = ft_set_flags_number();
	if (flag.width > ft_strlen(to_fill) && !flag.minus)
		padding.before = flag.width - ft_strlen(to_fill);
	else if (flag.minus || flag.width < 0)
	{
		if (flag.width < 0)
			flag.width *= -1;
		padding.after = flag.width - ft_strlen(to_fill);
	}
	if (flag.precision)
		padding.zero = flag.precision;
	count += padding_before(padding);
	count += ft_putstr_number(to_fill, padding);
	count += padding_after(padding);
	free(to_fill);
	return (count);
}

int				ft_put_ptr(void *ptr, t_arg flag)
{
	int				count;
	char			*to_fill;
	t_padding		padding;

	if (ptr == NULL || ptr == 0)
		return (ft_ptr_null(flag));
	if (!(to_fill = ft_itoa_p(ptr)))
		return (-1);
	padding = set_flags_ptr(&to_fill, flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putstr("0x");
	count += ft_putstr_number(to_fill, padding);
	count += padding_after(padding);
	free(to_fill);
	return (count);
}

//put s

t_padding	set_flags_s(char **to_fill, t_arg flag, t_padding padding)
{
	if (flag.width > ft_strlen(*to_fill) && !flag.minus)
		padding.before = flag.width - ft_strlen(*to_fill);
	else if (flag.width > ft_strlen(*to_fill) && flag.minus)
		padding.after = flag.width - ft_strlen(*to_fill);
	else if (flag.width < 0)
	{
		flag.width *= -1;
		if (flag.width > ft_strlen(*to_fill))
			padding.after = flag.width - ft_strlen(*to_fill);
	}
	return (padding);
}

int			ft_putstr(char *str)
{
	int			index;

	index = 0;
	while (str[index] != '\0')
	{
		ft_putchar(str[index]);
		index++;
	}
	return (index);
}

char		*check_s_null(t_arg flag)
{
	char		*to_fill;
	char		*tmp;

	if (!(to_fill = ft_strdup("(null)")))
		return (NULL);
	if (flag.precision < ft_strlen(to_fill) && flag.dot
		&& !(flag.precision < 0))
	{
		tmp = to_fill;
		if (!(to_fill = ft_substr(to_fill, 0, flag.precision)))
			return (NULL);
		free(tmp);
	}
	return (to_fill);
}

int			ft_put_s(char *str, t_arg flag)
{
	int			count;
	char		*to_fill;
	t_padding	padding;

	if (str == NULL)
	{
		if (!(to_fill = check_s_null(flag)))
			return (-1);
	}
	else if (flag.precision < ft_strlen(str) && flag.dot
			&& !(flag.precision < 0))
	{
		if (!(to_fill = ft_substr(str, 0, flag.precision)))
			return (-1);
	}
	else if (!(to_fill = ft_strdup(str)))
		return (-1);
	padding = set_flags_s(&to_fill, flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putstr(to_fill);
	count += padding_after(padding);
	free(to_fill);
	return (count);
}

// put u
int	ft_put_u(unsigned int num, t_arg flag)
{
	int			count;
	char		*to_fill;
	t_padding	padding;
	char		*tmp;

	if (!(to_fill = ft_itoa_u(num)))
		return (-1);
	if (num == 0 && flag.precision == 0 && flag.dot == 1)
	{
		tmp = to_fill;
		if (!(to_fill = ft_strdup("")))
		{
			free(tmp);
			return (-1);
		}
		free(tmp);
	}
	padding = set_flags(&to_fill, flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putstr_number(to_fill, padding);
	count += padding_after(padding);
	free(to_fill);
	return (count);
}

//put x
char		*ft_strjoin_x(char s1, char *s2)
{
	char		*str;
	int			i;
	int			j;

	i = 0;
	j = 1;
	if (!s1 && !s2)
		return (NULL);
	if (!(str = malloc(sizeof(char) * (ft_strlen(s2) + 2))))
		return (NULL);
	if (s1)
		str[0] = s1;
	while (s2[i])
		str[j++] = s2[i++];
	str[j] = '\0';
	return (str);
}

int			ft_put_x(unsigned int num, t_arg flag)
{
	int			count;
	char		*to_fill;
	t_padding	padding;

	if (num == 0 && !(flag.precision == 0 && flag.dot))
	{
		to_fill = ft_strdup("0");
		if (!to_fill)
			return (-1);
	}
	else if (!(to_fill = ft_itoa_x(num, flag)))
		return (-1);
	padding = set_flags(&to_fill, flag, ft_set_flags_number());
	count = 0;
	count += padding_before(padding);
	count += ft_putstr_number(to_fill, padding);
	count += padding_after(padding);
	free(to_fill);
	return (count);
}


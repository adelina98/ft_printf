#include "libftprintf.h"

int		ft_flag(int n)
{
	int	n_flag;

	n_flag = 0;
	if (n < 0)
		n_flag = 1;
	return (n_flag);
}

int		ft_digits(int dig, int n, int n_flag)
{
	while (n > 0)
	{
		dig += 1;
		n /= 10;
	}
	if (n_flag == 1)
		dig += 1;
	return (dig);
}

char	*ft_get_mem(int dig, int n, int n_flag)
{
	char	*str;

	if (!(str = (char*)malloc(sizeof(char) * (dig + 1))))
		return (NULL);
	str[dig] = '\0';
	if (n == 0)
		str[--dig] = 0 + '0';
	while (n > 0)
	{
		str[--dig] = n % 10 + '0';
		n /= 10;
	}
	if (n_flag > 0)
		str[0] = '-';
	return (str);
}

char	*ft_itoa(int n)
{
	int	dig;
	int	n_flag;

	dig = 0;
	n_flag = 0;
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	n_flag = ft_flag(n);
	if (n < 0)
		n *= -1;
	if (n == 0)
		dig = 1;
	else
		dig = ft_digits(dig, n, n_flag);
	return (ft_get_mem(dig, n, n_flag));
}

//itoa_u

int		ft_digits_u(int dig, unsigned int n)
{
	while (n)
	{
		dig += 1;
		n /= 10;
	}
	return (dig);
}

char	*ft_get_mem_u(int dig, unsigned int n)
{
	char	*str;

	if (!(str = (char*)malloc(sizeof(char) * (dig + 1))))
		return (NULL);
	str[dig] = '\0';
	if (n == 0)
		str[--dig] = 0 + '0';
	while (n > 0)
	{
		str[--dig] = n % 10 + '0';
		n /= 10;
	}
	return (str);
}

char	*ft_itoa_u(unsigned int n)
{
	int	dig;

	dig = 0;
	if (n == 0)
		dig = 1;
	else
		dig = ft_digits_u(dig, n);
	return (ft_get_mem_u(dig, n));
}

//itoa_x

char			*ft_itoa_x(unsigned int num, t_arg flag)
{
	char	*result;
	char	*tmp;
	char	hex;

	if (!(result = ft_strdup("")))
		return (NULL);
	while (num != 0)
	{
		if (num % 16 < 10)
			hex = num % 16 + 48;
		else if (flag.conversion == 'X')
			hex = num % 16 + 55;
		else
			hex = num % 16 + 55 + 32;
		tmp = result;
		if (!(result = ft_strjoin_x(hex, result)))
		{
			free(tmp);
			return (NULL);
		}
		free(tmp);
		num /= 16;
	}
	return (result);
}

